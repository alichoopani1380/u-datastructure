package datastructure.stack;

import datastructure.list.LinkedList;
import datastructure.list.linkedlist.LinkedListImpl;

public class ReverseLinkedListStack<T> implements Stack<T> {
    LinkedList<T> list;

    public ReverseLinkedListStack() {
        this.list = new LinkedListImpl<>();
    }

    @Override
    public void push(T data) {
        list.addFirst(data);
    }

    @Override
    public T pop() {
        return list.getAndRemoveFirst();
    }

    @Override
    public T peek() {
        return list.getFirst();
    }

    @Override
    public String toString() {
        return list.toString();
    }
}
