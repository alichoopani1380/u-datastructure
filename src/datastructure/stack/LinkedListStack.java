package datastructure.stack;

import datastructure.list.LinkedList;
import datastructure.list.linkedlist.LinkedListImpl;

public class LinkedListStack<T> implements Stack<T> {
    LinkedList<T> list;

    public LinkedListStack() {
        this.list = new LinkedListImpl<>();
    }

    @Override
    public void push(T data) {
        list.addLast(data);
    }

    @Override
    public T pop() {
        return list.getAndRemoveLast();
    }

    @Override
    public T peek() {
        return list.getLast();
    }

    @Override
    public String toString() {
        return list.toString();
    }
}
