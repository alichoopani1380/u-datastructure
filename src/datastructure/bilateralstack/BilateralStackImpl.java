package datastructure.bilateralstack;

import datastructure.list.LinkedList;
import datastructure.list.linkedlist.LinkedListImpl;

public class BilateralStackImpl<T> implements BilateralStack<T> {
    private LinkedList<T> leftList; //direction true
    private LinkedList<T> rightList; //direction false
    private Integer capacity;

    public BilateralStackImpl(Integer capacity) {
        this.capacity = capacity;
        leftList = new LinkedListImpl<>();
        rightList = new LinkedListImpl<>();
    }

    @Override
    public void push(Direction direction, T data) {
        if (hasCapacity())
            if (direction == Direction.LEFT)
                leftList.addFirst(data);
            else
                rightList.addFirst(data);
    }

    @Override
    public T pop(Direction direction) {
        return direction == Direction.LEFT ? leftList.getAndRemoveFirst() : rightList.getAndRemoveFirst();
    }

    @Override
    public Boolean hasCapacity() {
        return leftList.length() + rightList.length() < capacity;
    }

    @Override
    public String toString() {
        return leftList.toString() + " -- " + rightList.toString();
    }
}
