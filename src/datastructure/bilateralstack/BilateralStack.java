package datastructure.bilateralstack;

public interface BilateralStack<T> {

    void push(Direction direction, T data);

    T pop(Direction direction);

    Boolean hasCapacity();
}
