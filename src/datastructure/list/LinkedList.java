package datastructure.list;

import datastructure.list.linkedlist.NodeImpl;

public interface LinkedList<T> extends List<T> {
    NodeImpl<T> getFirstNode();
    NodeImpl<T> getLastNode();
    NodeImpl<T> getNthNode(Integer nth);
}
