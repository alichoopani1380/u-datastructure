package datastructure.list;

public interface List<T> {
    Integer length();
    void add(T data);
    void addFirst(T data);
    void addLast(T data);
    void addNth(Integer nth, T data);
    T getFirst();
    T getLast();
    T getNth(Integer nth);
    void removeFirst();
    void removeLast();
    void removeNth(Integer nth);
    T getAndRemoveFirst();
    T getAndRemoveLast();
    T getAndRemoveNth(Integer nth);
}
