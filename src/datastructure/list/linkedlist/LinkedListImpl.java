package datastructure.list.linkedlist;

import datastructure.list.LinkedList;
import datastructure.list.List;
import org.w3c.dom.Node;

public class LinkedListImpl<T> implements LinkedList<T> {
    private Integer index = -1;
    private NodeImpl<T> head;

    public LinkedListImpl(NodeImpl<T> head) {
        this.index = 0;
        this.head = head;
    }

    public LinkedListImpl() {
    }

    @Override
    public Integer length() {
        return index + 1;
    }

    @Override
    public void add(T data) {
        addLast(data);
    }

    @Override
    public void addFirst(T data) {
        head = index == -1 ? new NodeImpl<>(data) : new NodeImpl<>(data, head);
        ++index;
    }

    @Override
    public void addLast(T data) {
        if (index == -1) {
            head = new NodeImpl<>(data);
            index = 0;
            return;
        }
        getLastNode().setNextNode(new NodeImpl<>(data));
        ++index;
    }

    @Override
    public void addNth(Integer nth, T data) {
        if (index == nth + 1) {
            getLastNode().setNextNode(new NodeImpl<>(data));
            ++index;
        } else if (index.equals(nth)) {
            addLast(data);
        } else if (index > nth) {
            NodeImpl<T> tmp = getNthNode(nth - 1);
            NodeImpl<T> newNode = new NodeImpl<>(data, tmp.getNextNode());
            tmp.setNextNode(newNode);
            ++index;
        }
    }

    @Override
    public T getFirst() {
        NodeImpl<T> tmp = getFirstNode();
        return tmp == null ? null : tmp.getData();
    }

    @Override
    public T getLast() {
        NodeImpl<T> tmp = getLastNode();
        return tmp == null ? null : tmp.getData();
    }

    @Override
    public T getNth(Integer nth) {
        return getNthNode(nth).getData();
    }

    @Override
    public void removeFirst() {
        NodeImpl<T> tmp = getFirstNode();
        if (tmp == null) {
            return;
        }
        if (tmp.hasNextNode()) {
            head = tmp.getNextNode();
            --index;
            return;
        }
        head = null;
        index = -1;
    }

    @Override
    public void removeLast() {
        NodeImpl<T> tmp = getFirstNode();
        if (tmp == null) {
            return;
        }
        while (tmp.hasNextNode() && tmp.getNextNode().hasNextNode())
            tmp = tmp.getNextNode();
        tmp.setNextNode(null);
        --index;
    }

    @Override
    public void removeNth(Integer nth) {
        if (index == -1)
            return;
        if (nth > index)
            return;
        if (nth == 0) {
            removeFirst();
            return;
        }
        if (nth == index) {
            removeLast();
            return;
        }
        NodeImpl<T> tmp = getNthNode(nth - 1);
        tmp.setNextNode(tmp.getNextNode().getNextNode());
        --index;
    }

    @Override
    public T getAndRemoveFirst() {
        T tmp = getFirst();
        removeFirst();
        return tmp;
    }

    @Override
    public T getAndRemoveLast() {
        T tmp = getLast();
        removeLast();
        return tmp;
    }

    @Override
    public T getAndRemoveNth(Integer nth) {
        T tmp = getNth(nth);
        removeNth(nth);
        return tmp;
    }

    @Override
    public NodeImpl<T> getFirstNode() {
        if (index == -1)
            return null;
        return head;
    }

    @Override
    public NodeImpl<T> getLastNode() {
        NodeImpl<T> tmp = getFirstNode();
        if (index == -1 || tmp == null)
            return null;
        while (tmp.getNextNode() != null)
            tmp = tmp.getNextNode();
        return tmp;
    }

    @Override
    public NodeImpl<T> getNthNode(Integer nth) {
        if (index == -1 || nth > index)
            return new NodeImpl<>();
        NodeImpl<T> tmp = getFirstNode();
        for (int i = 0; i < nth; i++) {
            tmp = tmp.getNextNode();
        }
        return tmp;
    }

    @Override
    public String toString() {
        if (index == -1)
            return "[]";

        String out = "[";
        NodeImpl<T> tmp = getFirstNode();
        for (int i = 0; i <= index; i++) {
            out += tmp.getData() + ", ";
            tmp = tmp.getNextNode();
        }
        return out.substring(0, out.length() - 2) + "]";
    }
}
