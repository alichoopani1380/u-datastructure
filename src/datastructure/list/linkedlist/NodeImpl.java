package datastructure.list.linkedlist;

public class NodeImpl<T> {
    private T data;
    private NodeImpl<T> nextNode;

    public NodeImpl(T data, NodeImpl<T> nextNode) {
        this.data = data;
        this.nextNode = nextNode;
    }

    public NodeImpl(T data) {
        this.data = data;
    }

    public NodeImpl() {
    }

    public Boolean hasNextNode() {
        return nextNode != null;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public void setNextNode(NodeImpl<T> nextNode) {
        this.nextNode = nextNode;
    }

    public NodeImpl<T> getNextNode() {
        return nextNode;
    }

    @Override
    public String toString() {
        return data.toString();
    }
}
