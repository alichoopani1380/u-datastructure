package calculator;

import java.util.Arrays;
import java.util.List;

public class Util {

    public static boolean isOperand(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isOperator(Character s) {
        List<Character> operators = Arrays.asList('+', '-', '*', '/', '%');
        return operators.contains(s);
    }

    public static boolean hasPriority(Character i, Character j) {
        if (j == null)
            return false;
        return getPriority(i) < getPriority(j);
    }

    public static Integer getPriority(Character i) {
        if (Arrays.asList('*', '/', '%').contains(i))
            return 0;
        if (Arrays.asList('+', '-').contains(i))
            return 1;
        return null;
    }
}
