package calculator.convertor;

import datastructure.stack.ReverseLinkedListStack;
import datastructure.stack.Stack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import calculator.Util;

public class InfixToPostfix {

    private String inputStr;
    private String[] inputList;

    private Stack<Character> operandStack = new ReverseLinkedListStack<>();

    private List<String> out = new ArrayList<>();


    public InfixToPostfix(String s) {
        inputStr = s;
        inputList = s.split(" ");
    }

    public void convert() {
        for (String item : inputList) {
            if (Util.isOperand(item)) {
                out.add(item);
            } else if (Util.isOperator(item.charAt(0))) {
                if (!Util.hasPriority(item.charAt(0), operandStack.peek())) {
                    pushOperandStackToOut();
                }
                operandStack.push(item.charAt(0));
            }
        }
        pushOperandStackToOut();
    }

    private void pushOperandStackToOut() {
        Character tmp = operandStack.pop();
        while (tmp != null) {
            out.add(tmp.toString());
            tmp = operandStack.pop();
        }
    }

    public String getResultString() {
        return String.join(" ", out);
    }
}
