package calculator;

import calculator.convertor.InfixToPostfix;
import datastructure.stack.ReverseLinkedListStack;
import datastructure.stack.Stack;

public class Calculator {

    private String inputStr;
    private String[] inputList;

    private Stack<Integer> operandStack = new ReverseLinkedListStack<>();
    private Integer out;

    public Calculator(String str) {
        inputStr = str;
    }

    public static Calculator init(String str) {
        return new Calculator(str);
    }

    public Calculator fromPostfix() {
        inputList = inputStr.split(" ");

        Integer op1, op2;
        for (String item : inputList) {
            if (Util.isOperand(item)) {
                operandStack.push(Integer.parseInt(item));
            } else if (Util.isOperator(item.charAt(0))) {
                op2 = operandStack.pop();
                op1 = operandStack.pop();
                operandStack.push(singleOperatorCalculate(op1, op2, item.charAt(0)));
            }
        }
        out = operandStack.pop();
        return this;
    }

    private Integer singleOperatorCalculate(Integer op1, Integer op2, char charAt) {
        return switch (charAt) {
            case '+' -> op1 + op2;
            case '-' -> op1 - op2;
            case '*' -> op1 * op2;
            case '/' -> op1 / op2;
            case '%' -> op1 % op2;
            default -> 0;
        };
    }

    public Calculator fromInfix() {
        var convertor = new InfixToPostfix(inputStr);
        convertor.convert();
        inputStr = convertor.getResultString();
        fromPostfix();
        return this;
    }

    public Integer get() {
        return out;
    }
}
